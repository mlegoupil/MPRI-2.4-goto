(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-33-39"]

open Algebra
open Mu

(*****************************************************************)
(* Generic recursor                                              *)
(*****************************************************************)

module Fix (A : Alg2) = struct
  module MuF = Mu2 (A.F)
  open MuF

  let rec fix1 = function
      RConstr1 x -> A.alg1 fix1 fix2 x

  and fix2 = function
      RConstr2 y -> A.alg2 fix1 fix2 y
end
