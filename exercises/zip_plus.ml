(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-33-34-37-39"]

open Fold
open Stack

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgZip_Plus = struct
  module F = CStack
  module G = CStack

  type carrier = Stack.t

  let alg (type x y) (f : x * y -> carrier) (s1,s2) = match s1,s2 with
    | F.Empty , _ | _, G.Empty -> empty ()
    | F.Cons (m,ms), G.Cons (n,ns) -> push (m+n) (f (ms,ns))
end

(*****************************************************************)
(* Fixpoint                                                      *)
(*****************************************************************)
module F = Curryfix.Fix(AlgZip_Plus)
let zip_plus ms ns = F.fix (ms,ns)

(*****************************************************************)
(* Tests                                                      *)
(*****************************************************************)

let%test _ =
  let x1 = Spec.Empty in
  let x2 = Spec.Empty in
  from_stack (Spec.zip_plus x1 x2) = zip_plus (from_stack x1) (from_stack x2)

let%test _ =
  let x1 = Spec.Push (42, Spec.Empty) in
  let x2 = Spec.Empty in
  from_stack (Spec.zip_plus x1 x2) = zip_plus (from_stack x1) (from_stack x2)

let%test _ =
  let x1 = Spec.Empty in
  let x2 = Spec.Push (42, Spec.Empty) in
  from_stack (Spec.zip_plus x1 x2) = zip_plus (from_stack x1) (from_stack x2)

let%test _ =
  let x1 = Spec.Push (1, Spec.Push (2, Spec.Push (3, Spec.Empty))) in
  let x2 = Spec.Push (42, Spec.Empty) in
  from_stack (Spec.zip_plus x1 x2) = zip_plus (from_stack x1) (from_stack x2)

let%test _ =
  let x1 = Spec.Push (42, Spec.Empty) in
  let x2 = Spec.Push (1, Spec.Push (2, Spec.Push (3, Spec.Empty))) in
  from_stack (Spec.zip_plus x1 x2) = zip_plus (from_stack x1) (from_stack x2)

let%test _ =
  let x1 = Spec.Push (1, Spec.Push (2, Spec.Push (3, Spec.Empty))) in
  let x2 = Spec.Push (4, Spec.Push (1, Spec.Push (5, Spec.Empty))) in
  from_stack (Spec.zip_plus x1 x2) = zip_plus (from_stack x1) (from_stack x2)
