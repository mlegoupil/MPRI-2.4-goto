(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-34-37-39"]

open Fold
open Stack

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgTotal = struct
  module F = CStack

  type carrier = int

  let alg f x = match x with
    | F.Empty -> 0
    | F.Cons (n,ns) -> n + f ns
end

(*****************************************************************)
(* Fixpoint                                                       *)
(*****************************************************************)
module F = Catamorphism.Fix(AlgTotal)
                
let total = F.fix

(*****************************************************************)
(* Tests                                                         *)
(*****************************************************************)

let%test _ =
  let x = Spec.Empty in
  Spec.total x = total (from_stack x)

let%test _ =
  let x = Spec.Push (42, Spec.Empty) in
  Spec.total x = total (from_stack x)

let%test _ =
  let x = Spec.Push (1, Spec.Push (2, Spec.Push (3, Spec.Empty))) in
  Spec.total x = total (from_stack x)
