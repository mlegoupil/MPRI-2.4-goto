(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-34-37-39"]

open Fold
open Mu

module CNat = struct
  type 'a f = NYI

  let map _ _ = failwith "NYI"
end

module Nat = Mu (CNat)

let zero () = failwith "NYI"

let succ n = failwith "NYI"

let rec from_nat _ = failwith "NYI"
