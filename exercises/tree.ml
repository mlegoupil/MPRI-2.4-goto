(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-34-37-39"]

open Fold
open Mu

module CTree = struct
  type 'a f =
    Leaf | Node of 'a * int * 'a

  let map f x = match x with
    | Leaf -> Leaf
    | Node (l,v,r) -> Node (f l,v,f r)
end

module Tree = Mu (CTree)

let leaf () = Tree.Constr Leaf

let node l a r = Tree.Constr (Node (l,a,r))

let rec from_tree = function
  | Spec.Leaf -> leaf ()
  | Spec.Node (l,v,r) -> node (from_tree l) v (from_tree r)
