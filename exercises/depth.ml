(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-34-37-39"]

open Fold
open Tree

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgDepth = struct
  module F = CTree

  type carrier = int

  let alg f = function
    | CTree.Leaf -> 0
    | CTree.Node (l,_,r) -> 1 + max (f l) (f r)
end

(*****************************************************************)
(* Fixpoint                                                      *)
(*****************************************************************)

module F = Catamorphism.Fix(AlgDepth)
let depth = F.fix

(*****************************************************************)
(* Tests                                                         *)
(*****************************************************************)

let%test _ =
  let x = Spec.Leaf in
  Spec.depth x = depth (from_tree x)

let%test _ =
  let x = Spec.Node (Spec.Leaf, 1, Spec.Leaf) in
  Spec.depth x = depth (from_tree x)

let%test _ =
  let x = Spec.Node (Spec.Node (Spec.Leaf, 2, Spec.Leaf), 1, Spec.Leaf) in
  Spec.depth x = depth (from_tree x)

let%test _ =
  let x = Spec.Node (Spec.Leaf, 1, Spec.Node (Spec.Leaf, 2, Spec.Leaf)) in
  Spec.depth x = depth (from_tree x)
