(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-33-34-37-39"]

open Fold
open Nat

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgFib = struct
  module F = CNat

  type carrier1 = int

  type carrier2 = int

  let alg1 _fib _fib' = function
    | F.Zero -> 0
    | F.Succ n -> _fib' n

  let alg2 _fib _fib' = function
    | F.Zero -> 1
    | F.Succ n -> _fib n + _fib' n
end

(*****************************************************************)
(* Fixpoint                                                      *)
(*****************************************************************)
module F = Paramorphism.Fix(AlgFib)
let fib = F.fix1

(*****************************************************************)
(* Tests                                                         *)
(*****************************************************************)

let%test _ =
  let x = Spec.Zero in
  Spec.fib x = fib (from_nat x)

let%test _ =
  let x = Spec.Succ Spec.Zero in
  Spec.fib x = fib (from_nat x)

let%test _ =
  let x = Spec.Succ (Spec.Succ Spec.Zero) in
  Spec.fib x = fib (from_nat x)

let%test _ =
  let x = Spec.Succ (Spec.Succ (Spec.Succ Spec.Zero)) in
  Spec.fib x = fib (from_nat x)
