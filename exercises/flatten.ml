(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-33-34-37-39"]

open Fold
open Rose

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgFlatten = struct
  module F = CRose

  type carrier1 = int list

  type carrier2 = int list

  let alg1 _flattena _flattens = function
    F.RNode (n,ts) -> n :: _flattens ts

  let alg2 _flattena _flattens = function
    | F.RNil -> []
    | F.RCons (t,ts) -> _flattena t @ _flattens ts
end

(*****************************************************************)
(* Fixpoint                                                      *)
(*****************************************************************)
module F = Mutual.Fix(AlgFlatten)
let flattena = F.fix1

let flattens = F.fix2

(*****************************************************************)
(* Tests                                                         *)
(*****************************************************************)

let%test _ =
  let x = Spec.RNode (0, Spec.RNil) in
  Spec.flattena x = flattena (from_rose x)

let%test _ =
  let x = Spec.RNode (0, Spec.RCons (Spec.RNode (3, Spec.RNil), Spec.RNil)) in
  Spec.flattena x = flattena (from_rose x)

let%test _ =
  let x =
    Spec.RNode
      ( 0
      , Spec.RCons
          ( Spec.RNode (3, Spec.RNil)
          , Spec.RCons (Spec.RNode (2, Spec.RNil), Spec.RNil) ) )
  in
  Spec.flattena x = flattena (from_rose x)

let%test _ =
  let x =
    Spec.RNode
      ( 0
      , Spec.RCons
          ( Spec.RNode (3, Spec.RCons (Spec.RNode (1, Spec.RNil), Spec.RNil))
          , Spec.RCons (Spec.RNode (2, Spec.RNil), Spec.RNil) ) )
  in
  Spec.flattena x = flattena (from_rose x)
