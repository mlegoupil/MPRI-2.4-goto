(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-33-34-37-39"]

open Fold
open Stack

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgShunt = struct
  module F = CStack

  type carrier = Stack.t

  type ctxt = Stack.t

  let alg (type x) (f : x * Stack.t -> carrier) (ms,ns) =
    match ms with
      F.Empty -> ns
    | F.Cons (m,ms) -> f (ms, push m ns)
end

(*****************************************************************)
(* Fixpoint                                                      *)
(*****************************************************************)
module F = Curry.Fix(AlgShunt)
let shunt ms ns = F.fix(ms,ns)

(*****************************************************************)
(* Tests                                                         *)
(*****************************************************************)

let%test _ =
  let x1 = Spec.Empty in
  let x2 = Spec.Empty in
  from_stack (Spec.shunt x1 x2) = shunt (from_stack x1) (from_stack x2)

let%test _ =
  let x1 = Spec.Push (42, Spec.Empty) in
  let x2 = Spec.Empty in
  from_stack (Spec.shunt x1 x2) = shunt (from_stack x1) (from_stack x2)

let%test _ =
  let x1 = Spec.Empty in
  let x2 = Spec.Push (42, Spec.Empty) in
  from_stack (Spec.shunt x1 x2) = shunt (from_stack x1) (from_stack x2)

let%test _ =
  let x1 = Spec.Push (1, Spec.Push (2, Spec.Push (3, Spec.Empty))) in
  let x2 = Spec.Push (42, Spec.Empty) in
  from_stack (Spec.shunt x1 x2) = shunt (from_stack x1) (from_stack x2)
