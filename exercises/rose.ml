(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-34-37-39"]

open Fold
open Mu

module CRose = struct
  type ('a, 'b) f1 = RNode of int * 'b

  type ('a, 'b) f2 = RNil | RCons of 'a * 'b

  let map1 _ g = function
    | RNode (n,x) -> RNode (n,g x)

  let map2 f g = function
    | RNil -> RNil
    | RCons (a,b) -> RCons (f a, g b)
end

module Rose = Mu2 (CRose)

let node n ts = Rose.RConstr1 (RNode (n,ts))

let nil () = Rose.RConstr2 RNil

let cons t ts = Rose.RConstr2 (RCons (t,ts))

let rec from_rose = function
  | Spec.RNode (n,ts) -> node n (from_roses ts)

and from_roses = function
  | Spec.RNil -> nil ()
  | Spec.RCons (t,ts) -> cons (from_rose t) (from_roses ts)
