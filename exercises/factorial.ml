(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-33-34-37-39"]

open Fold
open Nat

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgFac = struct
  module F = CNat

  type carrier1 = int

  type carrier2 = int

  let alg1 _fac _id = function
    | F.Zero -> 1
    | F.Succ n -> (1 + _id n) * _fac n

  let alg2 _fac _id = function
    | F.Zero -> 0
    | F.Succ n -> 1 + _id n
end

(*****************************************************************)
(* Fixpoint                                                      *)
(*****************************************************************)
module F = Paramorphism.Fix(AlgFac)
let fac = F.fix1

(*****************************************************************)
(* Tests                                                         *)
(*****************************************************************)

let%test _ =
  let x = Spec.Zero in
  Spec.fac x = fac (from_nat x)

let%test _ =
  let x = Spec.Succ Spec.Zero in
  Spec.fac x = fac (from_nat x)

let%test _ =
  let x = Spec.Succ (Spec.Succ Spec.Zero) in
  Spec.fac x = fac (from_nat x)

let%test _ =
  let x = Spec.Succ (Spec.Succ (Spec.Succ Spec.Zero)) in
  Spec.fac x = fac (from_nat x)
