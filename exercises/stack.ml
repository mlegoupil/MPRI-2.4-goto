(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-34-37-39"]

open Fold
open Mu

module CStack = struct
  type 'a f = Empty | Cons of int * 'a

  let map f x = match x with
      Empty -> Empty
    | Cons (n,a) -> Cons (n,f a)
end

module Stack = Mu (CStack)

let empty () = Stack.Constr Empty

let push n ns = Stack.Constr (Cons (n, ns))

let rec from_stack s = match s with
  | Spec.Empty -> empty ()
  | Spec.Push (n,ns) -> push n (from_stack ns)
